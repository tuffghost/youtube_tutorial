﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


public class ThrowController : MonoBehaviour
{
    private Rigidbody weaponRb;
    private WeaponScript weaponScript;
    private float returnTime = 0;
    private Transform LastPosition;
    public OVRGrabbable WeaponGrab;   


    private Vector3 pullPosition;
    private Vector3 origLocRot;
    private Vector3 CurrentVelocity;

    [Header("Public References")]
    public Transform weapon;
    public Transform hand;
    public Transform curvePoint;
    
    [Space]
    [Header("Parameters")]
    public float throwPower = 30;
    [Space]
    [Header("Bools")]
    public bool hasWeapon = false;
    public bool pulling = false;


    void Start()
    {

        weaponRb = weapon.GetComponent<Rigidbody>();
        weaponScript = weapon.GetComponent<WeaponScript>();
        origLocRot = weapon.localEulerAngles;
        // WeaponGrab = weapon.GetComponent<OVRGrabbable>();

    }

    void Update()
    {
        LastPosition = weapon.transform;

            if (OVRInput.GetDown(OVRInput.Button.One))
            {
                WeaponStartPull();
            }
        
            if ((OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger) || (OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger))) && hasWeapon)
        {
            WeaponThrow();
        }

            if (WeaponGrab.isGrabbed)
        {
            hasWeapon = true;
        }
        if (pulling)
        {
            if (returnTime < 1)
            {
              
                weapon.position = GetQuadraticCurvePoint(returnTime, pullPosition, curvePoint.position, hand.position);
                returnTime += Time.deltaTime * 1.5f;
            }
           else
           {
               WeaponCatch();
            }
        }

    }

  

    public void WeaponThrow()
    {

        hasWeapon = false;
        weaponScript.activated = true;
        weaponRb.isKinematic = false;
        weaponRb.collisionDetectionMode = CollisionDetectionMode.Continuous;
       // weapon.parent = null;
        weapon.eulerAngles = new Vector3(0, -90 + transform.eulerAngles.y, 0);
        weapon.transform.position += transform.right / 5;
        CurrentVelocity = weapon.position - LastPosition.position;
        Debug.Log("weapon postion" + weapon.position);
        Debug.Log("weapon velocity rb" + weaponRb.velocity);
        weaponRb.AddForce(weaponRb.velocity * throwPower, ForceMode.Impulse);
        Debug.Log("CurrentVelocity" + CurrentVelocity);

    }

    public void WeaponStartPull()
    {
        pullPosition = weapon.position;
        weaponRb.Sleep();
        weaponRb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        weaponRb.isKinematic = true;
        weapon.DORotate(new Vector3(-90, -90, 0), .2f).SetEase(Ease.InOutSine);
        weapon.DOBlendableLocalRotateBy(Vector3.right * 90, .5f);
        weaponScript.activated = true;
        pulling = true;
    }

    public void WeaponCatch()
    {
        returnTime = 0;
        pulling = false;
        //weapon.parent = hand;
        weaponScript.activated = false;
        weapon.localEulerAngles = hand.localEulerAngles;
        weapon.localPosition = hand.position;
        hasWeapon = true;


    }

    public Vector3 GetQuadraticCurvePoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        return (uu * p0) + (2 * u * t * p1) + (tt * p2);
    }

}
